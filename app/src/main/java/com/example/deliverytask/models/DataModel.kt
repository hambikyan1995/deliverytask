package com.example.deliverytask.models

import java.util.*

data class DataModel(
    val id: Int,
    val lat: Double,
    val lng: Double,
    val city: String,
    val address: String,
    val timeFrom: Calendar,
    val timeTo: Calendar,
    val estimatedTime: Calendar,
    var status: Int = 1
)