package com.example.deliverytask.views

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.deliverytask.AppData
import com.example.deliverytask.databinding.ActivityMainBinding
import com.example.deliverytask.views.adapters.ViewPagerAdapter
import com.example.deliverytask.views.fragments.MapFragment
import com.example.deliverytask.views.fragments.StopsFragment

class MainActivity : AppCompatActivity() {
    private lateinit var mBinding: ActivityMainBinding
    private lateinit var mViewPager: ViewPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        initView()
    }

    private fun initView() {
        mViewPager =
            ViewPagerAdapter(
                supportFragmentManager
            )
        mViewPager.addFragment(StopsFragment(), "STOPS (${AppData.getData().size})")
        mViewPager.addFragment(MapFragment(), "MAP")
        mBinding.viewPager.offscreenPageLimit = 2
        mBinding.viewPager.adapter = mViewPager
        mBinding.tabLayout.setupWithViewPager(mBinding.viewPager)
    }

    fun getMapFragment(): MapFragment {
        return mViewPager.getItem(1) as MapFragment
    }

    fun scrollToMapFragment() {
        mBinding.viewPager.setCurrentItem(1, false)
    }
}
