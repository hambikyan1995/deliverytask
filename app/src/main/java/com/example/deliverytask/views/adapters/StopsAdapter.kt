package com.example.deliverytask.views.adapters

import android.annotation.SuppressLint
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.deliverytask.R
import com.example.deliverytask.databinding.AdapterItemFinishedStateBinding
import com.example.deliverytask.databinding.AdapterStopItemBinding
import com.example.deliverytask.enums.StopStatusEnum
import com.example.deliverytask.models.DataModel
import com.example.deliverytask.utils.DateTimeUtil

class StopsAdapter(
    private var mList: ArrayList<DataModel>,
    val onNavigateClick: (model: DataModel, position: Int) -> Unit,
    val onFinishClick: (model: DataModel, position: Int) -> Unit
) :
    RecyclerView.Adapter<StopsAdapter.ViewHolder>() {
    private val viewTypeItemOnGoing: Int = 121
    private val viewTypeItemFinish: Int = 122

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            viewTypeItemOnGoing -> {
                val binding = AdapterStopItemBinding.inflate(inflater, parent, false)
                ViewHolderOnGoingItem(binding)
            }
            else -> {
                val binding = AdapterItemFinishedStateBinding.inflate(inflater, parent, false)
                ViewHolderFishedItem(binding)
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = mList[position]
        if (getItemViewType(position) == viewTypeItemOnGoing) {
            (holder as ViewHolderOnGoingItem).bindView(model, position)
        } else {
            (holder as ViewHolderFishedItem).bindView(model)
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (mList[position].status == StopStatusEnum.OnGoing.int || mList[position].status == StopStatusEnum.Expired.int)
            viewTypeItemOnGoing
        else viewTypeItemFinish
    }

    open inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    inner class ViewHolderOnGoingItem(private val mBinding: AdapterStopItemBinding) :
        ViewHolder(mBinding.root) {
        private var stateIsClicked = false

        @SuppressLint("SetTextI18n")
        fun bindView(model: DataModel, position: Int) {
            mBinding.textPosition.text = (position + 1).toString()
            mBinding.textAddress.text = model.address
            mBinding.textId.text = model.id.toString()
            mBinding.textCity.text = model.city
            mBinding.textEstimatedTime.text =
                DateTimeUtil.convertDateToString(
                    model.estimatedTime.time,
                    DateTimeUtil.TIME_PATTERN
                )
            mBinding.textTimeFromTo.text =
                "${DateTimeUtil.convertDateToString(
                    model.timeFrom.time,
                    DateTimeUtil.TIME_PATTERN
                )}-" +
                        DateTimeUtil.convertDateToString(
                            model.timeTo.time,
                            DateTimeUtil.TIME_PATTERN
                        )

            if (model.estimatedTime.after(model.timeFrom) && model.estimatedTime.before(model.timeTo)) {
                mBinding.textEstimatedTime.setTextColor(
                    ContextCompat.getColor(
                        mBinding.root.context,
                        R.color.green
                    )
                )
                mBinding.imageError.visibility = View.GONE
            } else {
                mBinding.textEstimatedTime.setTextColor(
                    ContextCompat.getColor(
                        mBinding.root.context,
                        R.color.red
                    )
                )
                mBinding.imageError.visibility = View.VISIBLE
            }
            mBinding.root.setOnClickListener {
                stateIsClicked = !stateIsClicked
                mBinding.stateIsClicked = stateIsClicked
                if (stateIsClicked) {
                    mBinding.parentLayout.setPadding(
                        0,
                        mBinding.parentLayout.paddingTop,
                        0,
                        mBinding.parentLayout.paddingBottom
                    )
                    mBinding.parentLayout.requestLayout()
                    mBinding.layoutDetails.setBackgroundColor(
                        ContextCompat.getColor(
                            mBinding.root.context,
                            R.color.white
                        )
                    )
                } else {
                    mBinding.parentLayout.setPadding(
                        mBinding.parentLayout.paddingBottom, mBinding.parentLayout.paddingTop,
                        mBinding.parentLayout.paddingBottom, mBinding.parentLayout.paddingBottom
                    )
                    mBinding.parentLayout.requestLayout()
                    mBinding.layoutDetails.setBackgroundColor(
                        ContextCompat.getColor(
                            mBinding.root.context,
                            R.color.gray_2
                        )
                    )
                }
            }

            mBinding.imageFinish.setOnClickListener {
                model.status = StopStatusEnum.Finished.int
                onFinishClick(model, position)
                notifyItemChanged(position)
            }

            mBinding.textFinish.setOnClickListener {
                mBinding.imageFinish.callOnClick()
            }

            mBinding.imageNavigate.setOnClickListener {
                onNavigateClick.invoke(model, position)
            }

            mBinding.textNavigate.setOnClickListener {
                onNavigateClick.invoke(model, position)
            }
        }
    }

    inner class ViewHolderFishedItem(private val mBinding: AdapterItemFinishedStateBinding) :
        ViewHolder(mBinding.root) {
        fun bindView(model: DataModel) {
            mBinding.textAddress.text = model.address
            mBinding.textId.text = model.id.toString()
            mBinding.textCity.text = model.city
        }
    }
}
