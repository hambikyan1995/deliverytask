package com.example.deliverytask.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.deliverytask.AppData
import com.example.deliverytask.R
import com.example.deliverytask.databinding.FragmentStopsBinding
import com.example.deliverytask.enums.StopStatusEnum
import com.example.deliverytask.models.DataModel
import com.example.deliverytask.views.MainActivity
import com.example.deliverytask.views.adapters.StopsAdapter

class StopsFragment : Fragment(R.layout.fragment_stops) {
    private lateinit var mBinding: FragmentStopsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = FragmentStopsBinding.inflate(inflater)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setStopsAdapter()
    }

    private fun setStopsAdapter() {
        mBinding.recyclerView.adapter =
            StopsAdapter(AppData.getData(), { dataModel: DataModel, position: Int ->
                run {
                    (requireActivity() as MainActivity).getMapFragment()
                        .scrollToCurrentMarker(position)
                    (requireActivity() as MainActivity).scrollToMapFragment()
                }
            }, { dataModel: DataModel, position: Int ->
                run {
                    AppData.getData()[position].status = StopStatusEnum.Finished.int
                    (requireActivity() as MainActivity).getMapFragment()
                        .updateMarker(position)
                }
            })
    }
}