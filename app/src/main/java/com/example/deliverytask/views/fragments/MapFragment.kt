package com.example.deliverytask.views.fragments

import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.deliverytask.AppData
import com.example.deliverytask.R
import com.example.deliverytask.databinding.FragmentMapBinding
import com.example.deliverytask.enums.StopStatusEnum
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.ui.IconGenerator


class MapFragment : Fragment(), OnMapReadyCallback {
    private lateinit var mBinding: FragmentMapBinding
    private var mGoogleMap: GoogleMap? = null
    private val mDefaultZoom = 14f
    private val mMarkersMap by lazy { HashMap<Int, Marker?>() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = FragmentMapBinding.inflate(inflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mBinding.map.onCreate(savedInstanceState)
        mBinding.map.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mGoogleMap = googleMap

        mGoogleMap?.uiSettings?.let {
            it.isMyLocationButtonEnabled = false
            it.isMapToolbarEnabled = false
        }

        addMarkers()
    }

    private fun addMarkers() {
        val data = AppData.getData()
        moveCameraToCurrentLocation(LatLng(data[0].lat, data[0].lng), 0, mDefaultZoom)
        for (i in 0 until data.size) {
            val model = data[i]
            val marker = mGoogleMap?.addMarker(
                MarkerOptions()
                    .position(LatLng(model.lat, model.lng))
                    .icon(
                        BitmapDescriptorFactory.fromBitmap(
                            makeBitmap(
                                (i + 1).toString(),
                                model.status
                            )
                        )
                    )
            )
            mMarkersMap[i] = marker
        }
    }

    private fun makeBitmap(text: String, status: Int): Bitmap? {
        return IconGenerator(requireContext()).apply {
            setColor(getColorWithStatus(status))
            setTextAppearance(R.style.iconGenText)
        }
            .makeIcon(text)
    }

    private fun getColorWithStatus(status: Int): Int {
        return when (status) {
            StopStatusEnum.OnGoing.int -> ContextCompat.getColor(
                requireContext(),
                R.color.colorAccent
            )
            StopStatusEnum.Finished.int -> ContextCompat.getColor(requireContext(), R.color.blue)
            else -> ContextCompat.getColor(requireContext(), R.color.red)
        }
    }

    private fun moveCameraToCurrentLocation(latLng: LatLng?, duration: Int, zoom: Float) {
        if (latLng != null) {
            val cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, zoom)
            if (duration == 0)
                mGoogleMap?.animateCamera(cameraUpdate)
            else
                mGoogleMap?.animateCamera(cameraUpdate, duration, null)
        }
    }

    fun scrollToCurrentMarker(position: Int) {
        val marker = mMarkersMap[position]
        moveCameraToCurrentLocation(marker?.position, 300, mDefaultZoom + 5)
    }

    fun updateMarker(position: Int) {
        val marker = mMarkersMap[position]
        marker?.setIcon(
            BitmapDescriptorFactory.fromBitmap(
                makeBitmap(
                    (position + 1).toString(),
                    AppData.getData()[position].status
                )
            )
        )
    }

    override fun onResume() {
        super.onResume()
        mBinding.map.onResume()
    }

    override fun onPause() {
        super.onPause()
        mBinding.map.onPause()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mBinding.map.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mBinding.map.onLowMemory()
    }
}