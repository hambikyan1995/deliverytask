package com.example.deliverytask.utils

import android.annotation.SuppressLint
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

object DateTimeUtil {

    const val SERVER_DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ss"
    const val DAY_MONTH_YEAR_PATTERN = "d MMM yyyy"
    const val DAY_MONTH_YEAR_TIME_PATTERN = "d MMM yyyy, HH:mm"
    const val DAY_MONTH_TIME_PATTERN = "d MMM HH:mm"
    const val DAY_MONTH_PATTERN = "d MMM"
    const val TIME_PATTERN = "HH:mm"
    const val NOTIFICATION_TIME_PATTERN = "d MMM, hh:mm"

    fun convertServerDate(date: String, datePattern: String, convertingDatePattern: String): String {
        val sdf = SimpleDateFormat(datePattern, Locale.getDefault())
        sdf.timeZone = TimeZone.getDefault()

        val sdf1 = SimpleDateFormat(convertingDatePattern, Locale.getDefault())
        sdf1.timeZone = TimeZone.getDefault()

        return try {
            sdf1.format(sdf.parse(date)!!)
        } catch (e: ParseException) {
            e.printStackTrace()
            ""
        }
    }

    fun convertToServerStringDateToStringFormat(stringDate: String, pattern: String): String? {
        val date = convertStringToDate(stringDate, pattern)
        var formatetDate: String? = null
        date?.let { formatetDate = convertDateToString(date, pattern) }
        return formatetDate
    }

    fun convertToServerUTCDate(date: Date, convertingDatePattern: String): Date? {
        val sdf = SimpleDateFormat(convertingDatePattern, Locale.getDefault())
        sdf.timeZone = TimeZone.getTimeZone("UTC")
        val stringDate = sdf.format(date)
        val sdf2 = SimpleDateFormat(convertingDatePattern, Locale.getDefault())
        return try {
            sdf2.parse(stringDate)
        } catch (e: ParseException) {
            e.printStackTrace()
            Date()
        }
    }

    fun convertDateToString(date: Date, datePattern: String): String {
        val sdf = SimpleDateFormat(datePattern, Locale.getDefault())
        return sdf.format(date)
    }

    fun convertDateToStringUTC(date: Date, datePattern: String): String {
        val sdf = SimpleDateFormat(datePattern, Locale.getDefault())
        sdf.timeZone = TimeZone.getTimeZone("UTC")
        return sdf.format(date)
    }

    fun convertStringToDate(date: String, datePattern: String): Date? {
        val sdf = SimpleDateFormat(datePattern, Locale.getDefault())
        sdf.timeZone = TimeZone.getTimeZone("UTC")
        return try {
            sdf.parse(date)
        } catch (e: ParseException) {
            e.printStackTrace()
            Date()
        }
    }

    @SuppressLint("SimpleDateFormat")
    fun getDates(dateString1: String, dateString2: String): List<Date> {
        val dates = ArrayList<Date>()
        val df1 = SimpleDateFormat("yyyy-MM-dd")
        var date1: Date? = null
        var date2: Date? = null
        try {
            date1 = df1.parse(dateString1)
            date2 = df1.parse(dateString2)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val cal1 = Calendar.getInstance()
        cal1.time = date1!!
        val cal2 = Calendar.getInstance()
        cal2.time = date2!!
        while (!cal1.after(cal2)) {
            dates.add(cal1.time)
            cal1.add(Calendar.DATE, 1)
        }
        return dates
    }

    fun convertMillisToString(millis: Long): String {
        return if (TimeUnit.MILLISECONDS.toHours(millis) == 0L)
            String.format(
                Locale.getDefault(), "%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(
                    TimeUnit.MILLISECONDS.toHours(millis)
                ),
                TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(
                    TimeUnit.MILLISECONDS.toMinutes(millis)
                )
            )
        else
            String.format(
                Locale.getDefault(), "%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(
                    TimeUnit.MILLISECONDS.toHours(millis)
                )
            )
    }
}