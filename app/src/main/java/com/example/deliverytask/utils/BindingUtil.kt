package com.example.deliverytask.utils

import android.view.View
import androidx.databinding.BindingAdapter

object BindingUtil {

    @BindingAdapter("visibleIf")
    @JvmStatic
    fun visibleIf(anyView: View, show: Boolean) {
        anyView.visibility = if (show) View.VISIBLE else View.GONE
    }

    @BindingAdapter("expandHeight")
    @JvmStatic
    fun expandHeight(anyView: View, expand: Boolean) {
        if (expand)
            ViewUtil.expandHeight(anyView, 250)
        else  ViewUtil.collapseHeight(anyView, 250)
    }
}

