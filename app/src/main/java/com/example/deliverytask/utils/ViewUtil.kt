package com.example.deliverytask.utils

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.content.res.Resources
import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.os.Build
import android.view.View
import android.view.ViewGroup

object ViewUtil {

    fun pxToDp(px: Float): Float {
        val densityDpi = Resources.getSystem().displayMetrics.densityDpi
        return px / (densityDpi / 160f)
    }

    fun dpToPx(dp: Float): Int {
        val density = Resources.getSystem().displayMetrics.density
        return Math.round(dp * density)
    }

    fun setColorFilter(drawable: Drawable?, color: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            drawable?.colorFilter = BlendModeColorFilter(color, BlendMode.SRC_ATOP)
        } else {
            drawable?.setColorFilter(color, PorterDuff.Mode.SRC_ATOP)
        }
    }

    fun expandHeight(v: View, duration: Long, mListener: ValueAnimatorListener? = null): ValueAnimator {
        v.measure(
            View.MeasureSpec.makeMeasureSpec(v.rootView.width, View.MeasureSpec.EXACTLY),
            View.MeasureSpec.makeMeasureSpec(v.rootView.height, View.MeasureSpec.AT_MOST)
        )
        val targetHeight = v.measuredHeight

        val heightAnimator = ValueAnimator.ofInt(0, targetHeight)
        heightAnimator.addUpdateListener { animation ->
            v.layoutParams.height = animation.animatedValue as Int
            v.requestLayout()
        }
        heightAnimator.duration = duration
        heightAnimator.start()

        mListener?.let { listener ->
            heightAnimator.addUpdateListener {
                listener.onValueChange(it.animatedValue.toString().toFloat())
            }
        }
        return heightAnimator
    }

    fun expandHeightTo(v: View, to: Int, duration: Long, mListener: ValueAnimatorListener? = null) {
        v.measure(
            View.MeasureSpec.makeMeasureSpec(v.rootView.width, View.MeasureSpec.EXACTLY),
            View.MeasureSpec.makeMeasureSpec(v.rootView.height, View.MeasureSpec.AT_MOST)
        )
        val targetHeight = v.measuredHeight

        val heightAnimator = ValueAnimator.ofInt(0, to)
        heightAnimator.addUpdateListener { animation ->
            v.layoutParams.height = animation.animatedValue as Int
            v.requestLayout()
        }
        heightAnimator.duration = duration
        heightAnimator.start()

        mListener?.let { listener ->
            heightAnimator.addUpdateListener {
                listener.onValueChange(it.animatedValue.toString().toFloat())
            }
        }
    }

    fun collapseHeight(v: View, duration: Long, mListener: ValueAnimatorListener? = null) {
        val initialHeight = v.measuredHeight
        val heightAnimator = ValueAnimator.ofInt(0, initialHeight)
        heightAnimator.addUpdateListener { animation ->
            val animatedValue = animation.animatedValue as Int
            v.layoutParams.height = initialHeight - animatedValue
            v.requestLayout()
        }
        heightAnimator.duration = duration
        heightAnimator.start()

        mListener?.let { listener ->
            heightAnimator.addUpdateListener {
                listener.onValueChange(initialHeight - it.animatedValue.toString().toFloat())
            }
        }
    }

    fun expandWidth(v: View, duration: Long, mListener: ValueAnimatorListener? = null) {
        v.measure(
            View.MeasureSpec.makeMeasureSpec(v.rootView.width, View.MeasureSpec.EXACTLY),
            View.MeasureSpec.makeMeasureSpec(v.rootView.height, View.MeasureSpec.EXACTLY)
        )
        val targetWidth = v.measuredWidth

        val widthAnimator = ValueAnimator.ofInt(0, targetWidth)
        widthAnimator.addUpdateListener { animation ->
            v.layoutParams.width = animation.animatedValue as Int
            v.requestLayout()
        }
        widthAnimator.duration = duration
        widthAnimator.start()

        mListener?.let { listener ->
            widthAnimator.addUpdateListener {
                listener.onValueChange(it.animatedValue.toString().toFloat())
            }
        }
    }

    fun expandWidthUnspecified(v: View, duration: Long, mListener: ValueAnimatorListener? = null) {
        v.measure(
            View.MeasureSpec.makeMeasureSpec(v.rootView.width, View.MeasureSpec.UNSPECIFIED),
            View.MeasureSpec.makeMeasureSpec(v.rootView.height, View.MeasureSpec.UNSPECIFIED)
        )
        val targetWidth = v.measuredWidth

        val widthAnimator = ValueAnimator.ofInt(0, targetWidth)
        widthAnimator.addUpdateListener { animation ->
            v.layoutParams.width = animation.animatedValue as Int
            v.requestLayout()
        }
        widthAnimator.duration = duration
        widthAnimator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                super.onAnimationEnd(animation)
                v.layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT
                v.requestLayout()
            }
        })
        widthAnimator.start()

        mListener?.let { listener ->
            widthAnimator.addUpdateListener {
                listener.onValueChange(it.animatedValue.toString().toFloat())
            }
        }
    }

    fun collapseWidth(v: View, duration: Long, mListener: ValueAnimatorListener? = null) {
        val initialWidth = v.measuredHeight
        val widthAnimator = ValueAnimator.ofInt(0, initialWidth)
        widthAnimator.addUpdateListener { animation ->
            val animatedValue = animation.animatedValue as Int
            v.layoutParams.width = initialWidth - animatedValue
            v.requestLayout()
        }
        widthAnimator.duration = duration
        widthAnimator.start()

        mListener?.let { listener ->
            widthAnimator.addUpdateListener {
                listener.onValueChange(initialWidth - it.animatedValue.toString().toFloat())
            }
        }
    }

    interface ValueAnimatorListener {
        fun onValueChange(value: Float)
    }
}