package com.example.deliverytask.enums

enum class StopStatusEnum(val int: Int) {
    OnGoing(1),
    Finished(2),
    Expired(3),
}