package com.example.deliverytask

import com.example.deliverytask.enums.StopStatusEnum
import com.example.deliverytask.models.DataModel
import java.util.*

object AppData {
    private var data_: ArrayList<DataModel> = arrayListOf(
        DataModel(
            Random().nextInt(10000000),
            40.184924,
            44.515444,
            "Armenia,Yerevan",
            "Kenton",
            Calendar.getInstance(),
            Calendar.getInstance(),
            Calendar.getInstance(),
            StopStatusEnum.Finished.int
        ),
        DataModel(
            Random().nextInt(10000000), 40.180108, 44.508995, "Yerevan, Armenia", "11 Amiryan St",
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 8)
                set(Calendar.MINUTE, 0)
            },
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 10)
                set(Calendar.MINUTE, 0)
            },
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 11)
                set(Calendar.MINUTE, 0)
            },
            StopStatusEnum.Expired.int
        ),
        DataModel(
            Random().nextInt(10000000),
            40.179002,
            44.509768,
            "Armenia,Yerevan",
            "3-1 Zakyan St",
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 10)
                set(Calendar.MINUTE, 0)
            },
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 12)
                set(Calendar.MINUTE, 20)
            },
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 11)
                set(Calendar.MINUTE, 30)
            },
            StopStatusEnum.OnGoing.int
        ),
        DataModel(
            Random().nextInt(10000000),
            40.178403, 44.509599,
            "Armenia,Yerevan",
            "Ter-Grigoryan St",
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 12)
                set(Calendar.MINUTE, 0)
            },
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 14)
                set(Calendar.MINUTE, 10)
            },
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 11)
                set(Calendar.MINUTE, 10)
            },
            StopStatusEnum.OnGoing.int
        ),
        DataModel(
            Random().nextInt(10000000),
            40.178887, 44.510709,
            "Armenia,Yerevan",
            "4 Amiryan St",
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 10)
                set(Calendar.MINUTE, 0)
            },
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 12)
                set(Calendar.MINUTE, 50)
            },
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 11)
                set(Calendar.MINUTE, 0)
            },
            StopStatusEnum.Expired.int
        ),
        DataModel(
            Random().nextInt(10000000),
            40.178053, 44.511804,
            "Armenia,Yerevan",
            "Kenton",
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 16)
                set(Calendar.MINUTE, 0)
            },
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 18)
                set(Calendar.MINUTE, 10)
            },
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 17)
                set(Calendar.MINUTE, 0)
            },
            StopStatusEnum.OnGoing.int
        ),
        DataModel(
            Random().nextInt(10000000),
            40.173832, 44.512857,
            "Armenia,Yerevan",
            "Petros Adamyan St",
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 10)
                set(Calendar.MINUTE, 0)
            },
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 12)
                set(Calendar.MINUTE, 0)
            },
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 11)
                set(Calendar.MINUTE, 0)
            },
            StopStatusEnum.OnGoing.int
        ),

        DataModel(
            Random().nextInt(10000000),
            40.172200, 44.513624,
            "Armenia,Yerevan",
            "31 Tigran Mets Ave",
            Calendar.getInstance(),
            Calendar.getInstance(),
            Calendar.getInstance(),
            StopStatusEnum.Finished.int
        ),

        DataModel(
            Random().nextInt(10000000),
            40.170287, 44.514496,
            "Armenia,Yerevan",
            "Arhestavorner St",
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 16)
                set(Calendar.MINUTE, 0)
            },
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 20)
                set(Calendar.MINUTE, 0)
            },
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 18)
                set(Calendar.MINUTE, 0)
            },
            StopStatusEnum.OnGoing.int
        ),

        DataModel(
            Random().nextInt(10000000),
            40.178053, 44.511804,
            "Armenia,Yerevan",
            "Kenton",
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 10)
                set(Calendar.MINUTE, 0)
            },
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 12)
                set(Calendar.MINUTE, 0)
            },
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 11)
                set(Calendar.MINUTE, 0)
            },
            StopStatusEnum.OnGoing.int
        ),

        DataModel(
            Random().nextInt(10000000),
            40.169158, 44.514046,
            "Armenia,Yerevan",
            "Yervand Kochar St",
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 6)
                set(Calendar.MINUTE, 0)
            },
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 8)
                set(Calendar.MINUTE, 0)
            },
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 7)
                set(Calendar.MINUTE, 0)
            },
            StopStatusEnum.OnGoing.int
        ),

        DataModel(
            Random().nextInt(10000000),
            40.167863, 44.514220,
            "Armenia,Yerevan",
            "33 Marksi poxoc",
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 13)
                set(Calendar.MINUTE, 0)
            },
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 15)
                set(Calendar.MINUTE, 0)
            },
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 14)
                set(Calendar.MINUTE, 0)
            },
            StopStatusEnum.OnGoing.int
        ),

        DataModel(
            Random().nextInt(10000000),
            40.166804, 44.514180,
            "Armenia,Yerevan",
            "Zavarian Street",
            Calendar.getInstance(),
            Calendar.getInstance(),
            Calendar.getInstance(),
            StopStatusEnum.Finished.int
        ),

        DataModel(
            Random().nextInt(10000000),
            40.164130, 44.515717,
            "Armenia,Yerevan",
            "Kenton",
            Calendar.getInstance(),
            Calendar.getInstance(),
            Calendar.getInstance(),
            StopStatusEnum.Finished.int
        ),

        DataModel(
            Random().nextInt(10000000),
            40.162543, 44.516363,
            "Armenia,Yerevan",
            "Marksi poxoc",
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 10)
                set(Calendar.MINUTE, 0)
            },
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 12)
                set(Calendar.MINUTE, 0)
            },
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 14)
                set(Calendar.MINUTE, 0)
            },
            StopStatusEnum.Expired.int
        ),

        DataModel(
            Random().nextInt(10000000),
            40.161670, 44.514270,
            "Armenia,Yerevan",
            "10 Tigran Metz 1 back",
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 10)
                set(Calendar.MINUTE, 0)
            },
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 12)
                set(Calendar.MINUTE, 0)
            },
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 11)
                set(Calendar.MINUTE, 30)
            },
            StopStatusEnum.OnGoing.int
        ),

        DataModel(
            Random().nextInt(10000000),
            40.160021, 44.513188,
            "Armenia,Yerevan",
            "Glinka Back St",
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 6)
                set(Calendar.MINUTE, 0)
            },
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 7)
                set(Calendar.MINUTE, 30)
            },
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 8)
                set(Calendar.MINUTE, 0)
            },
            StopStatusEnum.OnGoing.int
        ),

        DataModel(
            Random().nextInt(10000000),
            40.156933, 44.513767,
            "Armenia,Yerevan",
            "Michurin St",
            Calendar.getInstance().apply { set(Calendar.HOUR_OF_DAY, 10) },
            Calendar.getInstance().apply { set(Calendar.HOUR_OF_DAY, 12) },
            Calendar.getInstance().apply { set(Calendar.HOUR_OF_DAY, 11) },
            StopStatusEnum.OnGoing.int
        ),

        DataModel(
            Random().nextInt(10000000),
            40.151364, 44.512498,
            "Armenia,Yerevan",
            "V. Achemian 1 back",
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 8)
                set(Calendar.MINUTE, 0)
            },
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 12)
                set(Calendar.MINUTE, 0)
            },
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 14)
                set(Calendar.MINUTE, 0)
            },
            StopStatusEnum.Expired.int
        ),

        DataModel(
            Random().nextInt(10000000),
            40.150290, 44.506607,
            "Armenia,Yerevan",
            "Վստրեչի կամուրջ",
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 10)
                set(Calendar.MINUTE, 0)
            },
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 12)
                set(Calendar.MINUTE, 0)
            },
            Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 11)
                set(Calendar.MINUTE, 0)
            },
            StopStatusEnum.OnGoing.int
        )
    )

    fun getData() = data_
}